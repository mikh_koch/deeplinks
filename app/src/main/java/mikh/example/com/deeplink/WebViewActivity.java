package mikh.example.com.deeplink;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

public class WebViewActivity extends AppCompatActivity {

    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        mWebView = findViewById(R.id.web_view);
        Intent intent = getIntent();
        Uri data = intent.getData();
        if(data != null) {
            mWebView.loadUrl(data.toString());
        }
    }

    @Override
    public void onDestroy() {
        mWebView.stopLoading();
        mWebView = null;
        super.onDestroy();
    }
}
